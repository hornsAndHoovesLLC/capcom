from django.apps import AppConfig


class CapcomConfig(AppConfig):
    name = 'capcom'
