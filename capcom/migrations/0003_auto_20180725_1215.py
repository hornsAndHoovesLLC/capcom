# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-07-25 09:15
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('capcom', '0002_auto_20180714_1352'),
    ]

    operations = [
        migrations.RenameField(
            model_name='capasitor_values',
            old_name='left_counter',
            new_name='mask1',
        ),
        migrations.RenameField(
            model_name='capasitor_values',
            old_name='right_counter',
            new_name='mask2',
        ),
        migrations.AddField(
            model_name='capasitor_values',
            name='capacity1',
            field=models.CharField(default=0, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='capasitor_values',
            name='capacity2',
            field=models.CharField(default=0, max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='capasitor_values',
            name='last_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 7, 25, 12, 15, 9, 963581)),
        ),
    ]
