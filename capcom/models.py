
import datetime

from django.db import models
from django.utils import timezone


class Capasitor_values(models.Model):
    mask1 = models.CharField(max_length=10)
    mask2 = models.CharField(max_length=10)
    capacity1 = models.CharField(max_length=100)
    capacity2 = models.CharField(max_length=100)
    comment = models.CharField(max_length=2000)
    last_date = models.DateTimeField(default=datetime.datetime.now())

    def __str__(self):
        return str(self.mask1) + ' _ ' + str(self.mask2)



