#!/usr/bin/python
from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from .models import Capasitor_values

from django.contrib.auth.models import User
from django.http import JsonResponse

#from capconnect import c_conn
import datetime
import subprocess
import sys
from time import sleep

from smbus import SMBus
import RPi.GPIO as gpio



def index(request):
    latest_question_list = Capasitor_values.objects.order_by('-last_date')#[:5]
    template = loader.get_template('capcom/index.html')
    context = RequestContext(request, {
        'latest_question_list': latest_question_list,
    })
    return HttpResponse(template.render(context))


def applyBinMasks(request):
    mask1 = request.GET.get('mask1', None)
    mask2 = request.GET.get('mask2', None)
    diapazon_mask = request.GET.get('diapazon_mask', None)
    capacity1 = request.GET.get('capacity1', None)
    capacity2 = request.GET.get('capacity2', None)


    #bytesL = bin(1)[2:].zfill(10)
    #bytesR = bin(2)[2:].zfill(10)

    bytesBinStr = mask1 + mask2 
    
    
    bytes_1 = bytesBinStr[13:] + '0'
    bytes_2 = bytesBinStr[6:13] + '0'
    bytes_3 = bytesBinStr[:6] + '0'

    bytes_d1 = diapazon_mask[:7] + '0'
    bytes_d2 = diapazon_mask[7:] + '0'
    

    bus = SMBus(1)

    #bus.write_byte(0x22, int('00000000', 2))    #take power off on diff plate
    #sleep(0.5)

    #bus.write_byte(0x23, int(bytes_1, 2))
    #bus.write_byte(0x24, int(bytes_2, 2))
    #bus.write_byte(0x25, int(bytes_3, 2))
    #bus.write_byte(0x20, int(bytes_d1, 2))
    #bus.write_byte(0x21, int(bytes_d2, 2))

    
    #sleep(0.5)
    #bus.write_byte(0x22, int('11000000', 2))    #take power on on diff plate


    pin = 4
    gpio.setmode(gpio.BCM)
    gpio.setwarnings(False)
    gpio.setup(pin ,gpio.OUT)

    #bus.write_byte(0x3a, int('00000000', 2))    #take power off on diff plate
    gpio.output(pin ,gpio.LOW)
    sleep(0.2)

    bus.write_byte(0x3c, int(bytes_1, 2))
    bus.write_byte(0x3d, int(bytes_2, 2))
    bus.write_byte(0x3a, int(bytes_3, 2))
    bus.write_byte(0x38, int(bytes_d1, 2))
    bus.write_byte(0x39, int(bytes_d2, 2))

    
    sleep(0.2)
    gpio.output(pin ,gpio.HIGH)
    #bus.write_byte(0x3a, int('11000000', 2))    #take power on on diff plate


    request.session['s_mask1'] = mask1
    request.session['s_mask2'] = mask2
    request.session['s_diapazon_mask'] = diapazon_mask
    request.session['s_capacity1'] = capacity1
    request.session['s_capacity2'] = capacity2


    data = {
        'mask1': mask1,
        'mask2': mask2,
        'diapazon_mask': diapazon_mask,
        'capacity1': capacity1,
        'capacity2': capacity2,
        'b1' : bytes_1,
        'b2' : bytes_2,
        'b3' : bytes_3,
        'bd1' : bytes_d1,
        'bd2' : bytes_d2

    }
    return JsonResponse(data)


def save_data(request):
    mask1 = request.GET.get('mask1',None)
    mask2 = request.GET.get('mask2', None)
    capacity1 = request.GET.get('capacity1', None)
    capacity2 = request.GET.get('capacity2', None)
    comment = request.GET.get('comment', None)

    val = Capasitor_values(mask1=mask1,mask2=mask2,capacity1=capacity1,capacity2=capacity2,comment=comment,last_date=datetime.datetime.now())
    val.save()

    data = {
        'resp': 'ok'
    }
    return JsonResponse(data)


def del_data(request):
    id_to_del = request.GET.get('id',None)

    Capasitor_values.objects.filter(id=id_to_del).delete()

    data = {
        'resp': 'ok'
    }
    return JsonResponse(data)


def get_session_masks(request):
    s_mask1 = request.session['s_mask1']
    s_mask2 = request.session['s_mask2']
    s_diapazon_mask = request.session['s_diapazon_mask']
    s_capacity1 = request.session['s_capacity1']
    s_capacity2 = request.session['s_capacity2']

    data = {
        'mask1': s_mask1,
        'mask2': s_mask2,
        'diapazon_mask': s_diapazon_mask,
        'capacity1': s_capacity1,
        'capacity2': s_capacity2
    }
    return JsonResponse(data)






