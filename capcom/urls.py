
from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views

from django.conf.urls import url

app_name = 'capcom'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^applyBinMasks/$', views.applyBinMasks, name='applyBinMasks'),
    url(r'^save_data/$', views.save_data, name='save_data'),
    url(r'^del_data/$', views.del_data, name='del_data'),
    url(r'^get_session_masks/$', views.get_session_masks, name='get_session_masks'),
]

urlpatterns += staticfiles_urlpatterns()

