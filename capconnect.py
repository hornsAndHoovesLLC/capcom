
#!/usr/bin/python
from smbus import SMBus
from itertools import cycle
from time import sleep

import sys
import argparse

# LED0 = [0, 0x01, 0.75, 0x20]
# LED1 = [1, 0x02, 1.5 , 0x20]
# LED2 = [2, 0x04, 3.0 , 0x20]
# LED3 = [3, 0x08, 6.0 , 0x20]
# LED4 = [4, 0x10, 12.0, 0x20]
# LED5 = [5, 0x20, 24.0, 0x20]
# LED6 = [6, 0x40, 48.0, 0x20]
#
# LED7 =  [7, 0x80, 96.0,  0x21]
# LED8 =  [8, 0x100, 192.0, 0x21]
# LED9 =  [9, 0x200, 384.0, 0x21]
# LED10 = [10,0x400, 768.0, 0x21]
# LED11 = [11,0x800, 1536.0,0x21]
# LED12 = [12,0x1000, 3072.0,0x21]
# LED13 = [13,0x2000, 6144.0,0x21]
#
# LED14 = [14, 0x4000, 12288.0,  0x22]
# LED15 = [15, 0x8000, 24576.0,  0x22]
# LED16 = [16, 0x10000, 49152.0,  0x22]
# LED17 = [17, 0x20000, 98304.0,  0x22]
# LED18 = [18, 0x40000, 196608.0, 0x22]
# LED19 = [19, 0x80000, 393216.0, 0x22]
# LED20 = [20, 0x0, 786432.0, 0x00]     #fake
# # 1048575 max sequence number
# LED_pwr = [20,0x00,0.0,0x22]

bus = SMBus(1)

parser = argparse.ArgumentParser()
parser.add_argument('-cap',type=int,dest='cap')

args=parser.parse_args()
cap_arg=args.cap
#print ledNum
#if cap_arg is None:
#    cap_arg=0

bytes=cap_arg#0x00

# PATTERN = (LED0,LED1,LED2,LED3,LED4,LED5,LED6,LED7,LED8,LED9,LED10,LED11,LED12,LED13,LED14,LED15,LED16,LED17,LED18,LED19,LED20)
# req_cap = float(cap_arg) * 0.75
# print(req_cap)
# cur_cap=0
# needed_cap = req_cap
# while cur_cap<req_cap:
#     for LED in cycle(PATTERN):
#         currLedNum=LED[0]
#         if needed_cap<LED[2]:
#             bytes=bytes|PATTERN[currLedNum-1][1]
#             cur_cap=cur_cap+PATTERN[currLedNum-1][2]
#             needed_cap=req_cap-cur_cap
#             print('increment ' + str(PATTERN[currLedNum-1][2]))
#             #print ('cur_cap ' + str(cur_cap))
#             break
#         if LED[0]==20:
#             print('error: not exist sequence number')
#             sys.exit()
#         #sleep(0.9)

#print ('bytes: ' + str(bytes))
bytesBin = bin(bytes)[2:].zfill(20)
#print('bytesBin: ' + bytesBin)
bytesBinStr = bytesBin


bytes_1 = bytesBinStr[13:]
#print('bytes_1: ' + bytes_1)

bytes_2 = bytesBinStr[6:13]
#print('bytes_2: ' + bytes_2)

bytes_3 = bytesBinStr[:6]  #6 bits
#print('bytes_3: ' + bytes_3)


#bus.write_byte(0x23, LED_pwr[1])    #take power off on diff plate


bus.write_byte(0x20, int(bytes_1, 2))
bus.write_byte(0x21, int(bytes_2, 2))
bus.write_byte(0x22, int(bytes_3, 2))



